(function($) {
	var content = '<div id="idletimeout">';
	
	content += ss.i18n.inject(
		ss.i18n._t('AutoLogout.NOTIFICATION', 'You will be logged off in {timer} seconds due to inactivity. <a id="idletimeout-resume" href="#">Click here to continue using this system</a>.'),
		{timer: '<span></span>'}
	);

	content += '</div>';

	$('body').append(content);
	
	$.idleTimeout('#idletimeout', '#idletimeout a', {
		idleAfter: $Timeout,
		keepAliveURL: 'Security/ping',
		serverResponseEquals: '1',
		AJAXTimeout: 3600,
		pollingInterval: $Interval,
		onTimeout: function(){
			$(this).slideUp();
			window.location = 'Security/logout';
		},
		onIdle: function(){
			$(this).slideDown();
		},
		onCountdown: function( counter ){
			$(this).find("span").html(counter);
		},
		onResume: function(){
			$(this).slideUp();
		}
	});
})(jQuery);