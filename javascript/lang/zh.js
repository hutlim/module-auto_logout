if ( typeof (ss) == 'undefined' || typeof (ss.i18n) == 'undefined') {
	console.error('Class ss.i18n not defined');
} else {
	ss.i18n.addDictionary('zh', {
		'AutoLogout.NOTIFICATION' : '由于闲置太久，您将在{timer}秒内被注销. <a id="idletimeout-resume" href="#">点击此处继续使用此系统</a>.'
	});
}