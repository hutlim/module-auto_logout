if ( typeof (ss) == 'undefined' || typeof (ss.i18n) == 'undefined') {
	console.error('Class ss.i18n not defined');
} else {
	ss.i18n.addDictionary('en', {
		'AutoLogout.NOTIFICATION' : 'You will be logged off in {timer} seconds due to inactivity. <a id="idletimeout-resume" href="#">Click here to continue using this system</a>.'
	});
}