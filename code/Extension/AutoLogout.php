<?php

class AutoLogout extends DataExtension {
	private static $timeout = 3600;
	private static $interval = 300;
	
	function contentcontrollerInit($controller){
        if(!$controller->redirectedTo() && !Distributor::customLoginID()){
			Requirements::javascript(THIRDPARTY_DIR . '/jquery/jquery.js');
			Requirements::javascript(FRAMEWORK_DIR . '/javascript/i18n.js');
			Requirements::add_i18n_javascript('auto_logout/javascript/lang');
			Requirements::javascript("auto_logout/thirdparty/jquery-idle-timeout/src/jquery.idletimeout.min.js");
			Requirements::javascript("auto_logout/thirdparty/jquery-idle-timeout/src/jquery.idletimer.js");
			Requirements::javascriptTemplate("auto_logout/javascript/AutoLogout.js", array('Timeout' => Config::inst()->get('AutoLogout', 'timeout'), 'Interval' => Config::inst()->get('AutoLogout', 'interval')));
			Requirements::css("auto_logout/css/AutoLogout.css");
		}
    }
}
